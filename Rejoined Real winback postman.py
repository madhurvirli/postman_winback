#!/usr/bin/env python
# coding: utf-8

# In[1]:

#
import pandas as pd
import numpy as np
from pandas.io import sql, gbq
import time, os
import datetime as dt
import mysql.connector as sqlcon
import os
import sys
import inspect
import psycopg2 as pg
import json
import mysql
from io import BytesIO
import requests
from io import BytesIO
import requests



root_path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))

# root_path = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
sys.path.append(root_path)
from utility_scripts import gmail_utility
from utility_scripts import formatting
from config import db_config as db

offset1 = 1
# offset3 = 1
offset2 = 0
offset4 = 330
offset5 = 30
offset6 = 60


dat1 = dt.datetime.now().date() - dt.timedelta(days = offset1)
dat2 = dt.datetime.now().date() - dt.timedelta(days = offset2)
dat3 = dt.datetime.now() + dt.timedelta(minutes = offset2)
dat5 = dt.datetime.now().date() - dt.timedelta(days = offset5)
dat_quality_check1 = dt.datetime.now().date() - dt.timedelta(days = offset5)
dat_quality_check2 = dt.datetime.now().date() - dt.timedelta(days = offset6)


RMS = db.credentials['RMS']
hl = db.credentials['hl_read']
frodo = db.credentials['frodo']
cps = db.credentials['CPS']
rissues = db.credentials['frodo_issues']
# ecom = db.credentials['Ecom']


cnx_rms = sqlcon.connect(host=RMS['host'], user=RMS['user'], 
                         password=RMS['password'], database=RMS['database'])
cnx2 = pg.connect(host = 'smaug-rep1.prod.db.shadowfax.in' , 
                  user = 'ananya.shankar' ,password = 'htYsqhJ' ,  database = 'smaugdb')
cnx_hl = sqlcon.connect(host=hl['host'], user=hl['user'], 
                         password=hl['password'], database=hl['database'])
cnx_frodo = pg.connect(host = frodo['host'], user = frodo['user'],
                       password = frodo['password'], database=frodo['database'])
cnx_cps = sqlcon.connect(host=cps['host'], user=cps['user'], 
                         password=cps['password'], database=cps['database'])
cnx_rissues = pg.connect(host=rissues['host'], user=rissues['user'], 
                         password=rissues['password'], database=rissues['database'])
cnx_swiggy = mysql.connector.connect(user='shadowfax', password='shadowfax123',
                         database= 'swiggy', host='test.prod.db.shadowfax.in')

dat1
print(dat1)
print(dat2)
print(dat3)
print(dat5)


# In[2]:


dat1


# In[3]:


import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd

scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']

credentials = ServiceAccountCredentials.from_json_keyfile_name(
         '/Users/hp/Downloads/sheets-and-jupyter-288806-ef9b502072f4.json', scope) # Your json file here


# In[4]:


gc = gspread.authorize(credentials)


# In[5]:


wks = gc.open("FK Raw-Data Sep'20")


# In[6]:


sheet2 = wks.get_worksheet(1)


# In[7]:


sheet2


# In[8]:


data = sheet2.get_all_values()
data.pop(0)
headers = data.pop(0)
df = pd.DataFrame(data, columns=headers)
df


# In[9]:


# df['Remarks 1 '].value_counts()


# In[10]:


# data[:2]


# In[11]:


df=df.iloc[:4513,:14]
df


# In[12]:


df.to_csv('Base_data.csv')


# In[13]:


dat1=str(dat1)
dat2=str(dat2)
dat1


# In[14]:


# df['Date of Call']= pd.to_datetime(df['Date of Call'])


# In[15]:


df1=df[df["Remarks 1 "].isin(["Interested"])].reset_index(drop=True)
dfintrstdystrd=df1[df1["Date of Call"].isin([dat1])].reset_index(drop=True)
dfintrstdtoday=df1[df1["Date of Call"].isin([dat2])].reset_index(drop=True)
dfintrstdtemp=df1[df1["Date of Call"].isin([dat1,'13-09-2020','13 -09-2020'])].reset_index(drop=True)


# In[16]:


# dfintrstdtemp


# In[17]:


df1


# In[82]:


df1['Date of Call'].values


# In[18]:


dfintrstdystrd


# In[19]:


dfintrstdystrd.to_csv('Interested_Winback_Postman_ystrd.csv')


# In[20]:


dfintrstdtoday


# In[21]:


# dfintrstdystrdtemp


# In[22]:


# df['Date of Call'].value_counts()


# In[23]:


# df['Zone'].value_counts()


# In[24]:


# df.count()


# In[25]:


# df2datetemp=df[df["Date of Call"].isin([dat1,'2020-08-09'])].reset_index(drop = True)
# df2datetemp.head()


# In[26]:


df2date=df[df["Date of Call"].isin([dat1])].reset_index(drop = True)
df2date


# In[27]:


dfdate = pd.DataFrame({'Date':  [dat1],
        'Interested': [0],
          'Not Interested': [0],
          'Other': [0]
          },)


# In[28]:


dfdate


# In[29]:


for i in range(len(df2date)):
    if df2date.loc[i, 'Remarks 1 '] == 'Interested':
        dfdate.loc[0, 'Interested'] += 1
    elif df2date.loc[i, 'Remarks 1 '] == 'Not interested':
        dfdate.loc[0, 'Not Interested'] += 1
    else :
        dfdate.loc[0, 'Other'] += 1


# In[30]:


dfdate.Date = pd.to_datetime(dfdate.Date)
dfdate


# In[31]:


dfzone = pd.DataFrame({'Zone':  ['East','West','South','North', '#N/A'],
        'Interested':  [0,0,0,0, 0],
          'Not Interested':  [0,0,0,0, 0],
          'Other':  [0,0,0,0, 0]
          })


# In[32]:


dfzone=dfzone.set_index('Zone')
dfzone


# In[33]:


for i in range(len(df)):
    zone = df.loc[i, 'Zone']
    # zone is an object, zone[0] returns string
    if df.loc[i, 'Remarks 1 '] == 'Interested':
        dfzone.loc[zone, 'Interested'] += 1
    elif df.loc[i, 'Remarks 1 '] == 'Not interested':
        dfzone.loc[zone, 'Not Interested'] += 1
    else :
        dfzone.loc[zone, 'Other'] += 1


# In[34]:


# df['Remarks 1 '].value_counts()


# In[35]:


dfzone


# In[36]:


dfzone=dfzone.reset_index()
dfzone


# In[37]:


dfzonedate = pd.DataFrame({'Zone':  ['East','West','South','North', '#N/A'],
        'Interested':  [0,0,0,0,0],
          'Not Interested':  [0,0,0,0,0],
          'Other':  [0,0,0,0,0]
          })


# In[38]:


dfzonedate


# In[39]:


dfzonedate=dfzonedate.set_index('Zone')
dfzonedate


# In[40]:


for i in range(len(df2date)):
    zone = df2date.loc[i, 'Zone']
    # zone is an object, zone[0] returns string
    if df2date.loc[i, 'Remarks 1 '] == 'Interested':
        dfzonedate.loc[zone, 'Interested'] += 1
    elif df2date.loc[i, 'Remarks 1 '] == 'Not interested':
        dfzonedate.loc[zone, 'Not Interested'] += 1
    else :
        dfzonedate.loc[zone, 'Other'] += 1


# In[41]:


dfzonedate


# In[42]:


# dfintrstdystrdtemp.to_csv('Interested Winback Postman (yesterday).csv',sep = ',',encoding = 'utf-8',index = False)
# df.to_csv('Base_data.csv',sep = ',',encoding = 'utf-8',index = False)
# # dfdate.to_csv('YSTRD_Date Winback Postman.csv',sep = ',',encoding = 'utf-8',index = False)
# # dfzone.to_csv('Zone Winback Postman.csv',sep = ',',encoding = 'utf-8',index = False)
# # file_list = ['Interested Winback Postman.csv','YSTRD_Date Winback Postman.csv','Zone Winback Postman.csv']
# # html_greeting="Hi All"
# # mail_subject= ' Interested, YSTRD_Date, Zone Winback Report  -'+time.strftime("%Y-%m-%d")

# # gmail_utility.send_email(['madhur.virli@shadowfax.in'],[], [],reply_to='',from_user = 'reports', subject=mail_subject,
# #                   mimetype_parts_dict={'html':html_greeting},attach_multi=file_list)


# In[43]:


# dfintrstdtemp.to_csv('Interested_last3days_winback_postman.csv',sep = ',',encoding = 'utf-8',index = False)

# file_list = ['Interested_last3days_winback_postman.csv']
# html_greeting="Hi Karan, Attaching the interested leads for last 3 days. PFA"
# mail_subject= ' Interested Winback Report  -'+time.strftime("%Y-%m-%d")

# gmail_utility.send_email(['madhur.virli@shadowfax.in'],['karan.rao@shadowfax.in'], [],reply_to='',from_user = 'reports', subject=mail_subject,
#                   mimetype_parts_dict={'html':html_greeting},attach_multi=file_list)


# In[44]:


# writer = pd.ExcelWriter('Winback Postman Summary.xlsx')
# dfdate.to_excel(writer, sheet_name= 'Yesterday Summary', index=True)
# dfzone.to_excel(writer, sheet_name='Zone Summary', index = True)
# writer.save()
# d = dt.date.today()
# # In[56]:
# attachment_list = ['Winback Postman Summary.xlsx','Interested Winback Postman (yesterday).csv','Base_data.csv']
# gmail_utility.send_email(['madhur.virli@shadowfax.in','aks@shadowfax.in','karan.rao@shadowfax.in','sawan.panicker@shadowfax.in','vijay.gurram@shadowfax.in','kumaresan.b@shadowfax.in'],[],[],'Winback Postman Report for '+str(d),
#     mimetype_parts_dict={'html': "Hi Team, <br><br>" + \
#                                  "Please find the Summaries below. I have also attached the base data and the details of leads who are interested. PFA.<br><br>"+ \
#                                  "<b><u> Winback Yesterday Summary: "  + " </u></b><br><br>" + \
#                                  formatting.apply_generic_css2(dfdate.to_html(index=True)) +
#                                  "<br><br><b><u> Winback Zone Summary:  "  + " </u></b><br><br>" + \
#                                  formatting.apply_generic_css2(dfzone.to_html(index=True)) +
#                                  "<br><br>"
# }, attach_multi=attachment_list, from_user='reports')


# In[45]:


a ='''select oc.rider_id, oc.date, oc.completed_count, oc.attempted_count  from   postman_ordercount oc'''

a = sql.read_sql(a,cnx_hl)
a.head()


# In[46]:


b = pd.read_csv(r"C:\Users\hp\Downloads\postman_attrition (1).csv")
b.head()


# In[47]:


ab = pd.merge(a,b,how='inner',left_on ='rider_id',right_on ='rider_id')
ab


# In[48]:


ab['rider_id']=ab['rider_id'].astype(int)
ab.date = pd.to_datetime(ab.date)
ab.lod = pd.to_datetime(ab.lod)


# In[49]:


import datetime


# In[50]:


date_time_obj = datetime.datetime.strptime('2020-09-05 00:00:00.00', '%Y-%m-%d %H:%M:%S.%f')


# In[51]:


# ab=ab.iloc[ab.groupby('rider_id')['date'].agg(pd.Series.idxmin)]


# In[52]:


ab1=ab.loc[(ab['date']>ab['lod']) & (ab['date']> date_time_obj)].reset_index()
ab1


# In[53]:


c ='''select c.id rider_id, d.name city, f.name zone from RMS_riderinfo c
left join RMS_city d on c.city = d.id
left join RMS_subclient e on c.subclient_id = e.id
left join RMS_zone f on d.zone_id=f.id'''

c = sql.read_sql(c,cnx_rms)
c.head()


# In[54]:


ab2=ab1.loc[ab1.groupby('rider_id')['date'].agg(pd.Series.idxmin)].reset_index()


# In[55]:


ab2


# In[56]:


abc = pd.merge(ab2,c,how='left',left_on ='rider_id',right_on ='rider_id')
abc


# In[57]:


abc1 = abc[['rider_id','date','lod','city','zone']]
abc1


# In[58]:


abc2=abc1[abc1["date"].isin([dat1])].reset_index(drop=True)
abc2


# In[59]:


dfdaterejoined = pd.DataFrame({'Date':  [dat1],
        '1st order': [0]
          },)
dfdaterejoined


# In[60]:


for i in range(len(abc2)):
    dfdaterejoined.loc[0, '1st order'] += 1
   


# In[61]:


dfdaterejoined


# In[62]:


dfzonerejoined = pd.DataFrame({'Zone':  ['East','West','South','North', '#N/A'],
        '1st order':  [0,0,0,0, 0]
          })
dfzonerejoined


# In[63]:


dfzonerejoined=dfzonerejoined.set_index('Zone')
dfzonerejoined


# In[64]:


for i in range(len(abc1)):
    zone = abc1.loc[i, 'zone']
    # zone is an object, zone[0] returns string
    dfzonerejoined.loc[zone, '1st order'] += 1


# In[65]:


dfzonerejoined=dfzonerejoined.reset_index()
dfzonerejoined


# In[66]:


dfzonedaterejoined = pd.DataFrame({'Zone':  ['East','West','South','North', '#N/A'],
        '1st order':  [0,0,0,0, 0]
          })
dfzonedaterejoined


# In[67]:


dfzonedaterejoined=dfzonedaterejoined.set_index('Zone')


# In[68]:


dfzonedaterejoined


# In[69]:


for i in range(len(abc2)):
    zone = abc2.loc[i, 'zone']
    # zone is an object, zone[0] returns string
    dfzonedaterejoined.loc[zone, '1st order'] += 1


# In[70]:


dfzonedaterejoined


# In[71]:


dfzonedaterejoined=dfzonedaterejoined.reset_index()
dfzonedaterejoined


# In[72]:


dfdaterejoined.Date = pd.to_datetime(dfdaterejoined.Date).apply(lambda x: str(x))
dfdate.Date = pd.to_datetime(dfdate.Date).apply(lambda x: str(x))


# In[73]:


dfdaterejoined


# In[74]:


dfdate


# In[75]:


dfdatefinal = pd.merge(dfdate,dfdaterejoined,how='left',left_on ='Date',right_on ='Date')
dfdatefinal.Date = pd.to_datetime(dfdatefinal.Date)
dfdatefinal


# In[76]:


dfzonefinal = pd.merge(dfzone,dfzonerejoined,how='left',left_on ='Zone',right_on ='Zone')
dfzonefinal


# In[77]:


dfzonedatefinal = pd.merge(dfzonedate,dfzonedaterejoined,how='left',left_on ='Zone',right_on ='Zone')
dfzonedatefinal


# In[ ]:


writer = pd.ExcelWriter('Winback Postman Summary.xlsx')
dfdatefinal.to_excel(writer, sheet_name= 'Yesterday Summary', index=True)
dfzonedatefinal.to_excel(writer, sheet_name='Yesterday Zone Summary', index = True)
dfzonefinal.to_excel(writer, sheet_name='Overall Zone Summary', index = True)
writer.save()
d = dt.date.today()
# In[56]:
attachment_list = ['Winback Postman Summary.xlsx','Interested_Winback_Postman_ystrd.csv','Base_data.csv']
gmail_utility.send_email(['madhur.virli@shadowfax.in'],[],[],'Winback Postman Report for '+str(d),
    mimetype_parts_dict={'html': "Hi Team, <br><br>" + \
                                 "Please find the Summaries below. Attaching yesterday interested leads and base data too. PFA. <br><br>"+ \
                                 "<b><u> Winback Yesterday Summary: "  + " </u></b><br><br>" + \
                                 formatting.apply_generic_css2(dfdatefinal.to_html(index=True)) +
                                 "<br><br><b><u> Winback Zone Yesterday Summary: "  + " </u></b><br><br>" + \
                                 formatting.apply_generic_css2(dfzonedatefinal.to_html(index=True)) +
                                 "<br><br><b><u> Winback Overall Zone Summary:   "  + " </u></b><br><br>" + \
                                 formatting.apply_generic_css2(dfzonefinal.to_html(index=True)) +
                                 "<br><br>"
}, attach_multi=attachment_list, from_user='reports')


# In[78]:


# writer = pd.ExcelWriter('Winback Postman Summary.xlsx')
# dfdatefinal.to_excel(writer, sheet_name= 'Yesterday Summary', index=True)
# dfzonedatefinal.to_excel(writer, sheet_name='Yesterday Zone Summary', index = True)
# dfzonefinal.to_excel(writer, sheet_name='Overall Zone Summary', index = True)
# writer.save()
# d = dt.date.today()
# # In[56]:
# attachment_list = ['Winback Postman Summary.xlsx','Interested_Winback_Postman_ystrd.csv','Base_data.csv']
# gmail_utility.send_email(['madhur.virli@shadowfax.in','aks@shadowfax.in','karan.rao@shadowfax.in','sawan.panicker@shadowfax.in','vijay.gurram@shadowfax.in','kumaresan.b@shadowfax.in'],[],[],'Winback Postman Report for '+str(d),
#     mimetype_parts_dict={'html': "Hi Team, <br><br>" + \
#                                  "Please find the Summaries below. Attaching yesterday interested leads and base data too. PFA. <br><br>"+ \
#                                  "<b><u> Winback Yesterday Summary: "  + " </u></b><br><br>" + \
#                                  formatting.apply_generic_css2(dfdatefinal.to_html(index=True)) +
#                                  "<br><br><b><u> Winback Zone Yesterday Summary: "  + " </u></b><br><br>" + \
#                                  formatting.apply_generic_css2(dfzonedatefinal.to_html(index=True)) +
#                                  "<br><br><b><u> Winback Overall Zone Summary:   "  + " </u></b><br><br>" + \
#                                  formatting.apply_generic_css2(dfzonefinal.to_html(index=True)) +
#                                  "<br><br>"
# }, attach_multi=attachment_list, from_user='reports')


# In[79]:


# writer = pd.ExcelWriter('Winback Postman Summary.xlsx')
# dfdate.to_excel(writer, sheet_name= 'Yesterday Summary', index=True)
# dfzone.to_excel(writer, sheet_name='Zone Summary', index = True)
# writer.save()
# d = dt.date.today()
# # In[56]:
# attachment_list = ['Winback Postman Summary.xlsx','Interested Winback Postman (yesterday).csv','Base_data.csv']
# gmail_utility.send_email(['madhur.virli@shadowfax.in','aks@shadowfax.in','karan.rao@shadowfax.in','sawan.panicker@shadowfax.in','vijay.gurram@shadowfax.in','kumaresan.b@shadowfax.in'],[],[],'Winback Postman Report for '+str(d),
#     mimetype_parts_dict={'html': "Hi Team, <br><br>" + \
#                                  "Please find the Summaries below. I have also attached the base data and the details of leads who are interested. PFA.<br><br>"+ \
#                                  "<b><u> Winback Yesterday Summary: "  + " </u></b><br><br>" + \
#                                  formatting.apply_generic_css2(dfdate.to_html(index=True)) +
#                                  "<br><br><b><u> Winback Zone Summary:  "  + " </u></b><br><br>" + \
#                                  formatting.apply_generic_css2(dfzone.to_html(index=True)) +
#                                  "<br><br>"
# }, attach_multi=attachment_list, from_user='reports')

