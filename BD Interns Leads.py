#!/usr/bin/env python
# coding: utf-8

# In[61]:
# new comment
# new comment 2

import pandas as pd
import numpy as np
from pandas.io import sql, gbq
import time, os
import datetime as dt
import mysql.connector as sqlcon
import os
import sys
import inspect
import psycopg2 as pg
import json
import mysql
from flask_sqlalchemy import sqlalchemy


root_path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))

# root_path = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
sys.path.append(root_path)
from utility_scripts import gmail_utility
from utility_scripts import formatting
from config import db_config as db

offset1 = 1
offset3 = 90
offset2 = 0
offset4 = 330
offset5 = 30
offset6 = 60


date1 = dt.datetime.now().date() - dt.timedelta(days = offset1)
dat3 = dt.datetime.now() - dt.timedelta(days = offset3)
dat5 = dt.datetime.now().date() - dt.timedelta(days = offset5)
dat_quality_check1 = dt.datetime.now().date() - dt.timedelta(days = offset5)
dat_quality_check2 = dt.datetime.now().date() - dt.timedelta(days = offset6)


RMS = db.credentials['RMS']
hl = db.credentials['hl_read']
frodo = db.credentials['frodo']
cps = db.credentials['CPS']
rissues = db.credentials['frodo_issues']
# ecom = db.credentials['Ecom']


cnx_rms = sqlcon.connect(host=RMS['host'], user=RMS['user'], 
                         password=RMS['password'], database=RMS['database'])
cnx2 = pg.connect(host = 'smaug-rep1.prod.db.shadowfax.in' , 
                  user = 'ananya.shankar' ,password = 'htYsqhJ' ,  database = 'smaugdb')
cnx_hl = sqlcon.connect(host=hl['host'], user=hl['user'], 
                         password=hl['password'], database=hl['database'])
cnx_frodo = pg.connect(host = frodo['host'], user = frodo['user'],
                       password = frodo['password'], database=frodo['database'])
cnx_cps = sqlcon.connect(host=cps['host'], user=cps['user'], 
                         password=cps['password'], database=cps['database'])
cnx_rissues = pg.connect(host=rissues['host'], user=rissues['user'], 
                         password=rissues['password'], database=rissues['database'])
cnx_swiggy = mysql.connector.connect(user='shadowfax', password='shadowfax123',
                         database= 'swiggy', host='test.prod.db.shadowfax.in')

# dat1
print(date1)
print(dat3)
print(dat5)


# In[51]:


# !pip install -U Flask-SQLAlchemy


# In[62]:


query = '''
    select a.id as lead_id, a.created, a.referring_rider_id, a.joined_rider_id, a.event_status, a.lead_status, a.lead_source_id, date(a.created) lead_date, 
    c.attempted_count, c.last_attempt_time, d.connected_count, d.last_connected_time, 
    e.interested_count, e.last_interested_time,

    b.name as source_name, a.city as city_id,  case when a.city = 5 then 1 when a.city = 8 then 2 when a.city = 7 then 3
    when a.city = 9 then 4 when a.city in (1,2,3) then 5 when a.city in (4,12) then 6 else 7 end as city_priority
    from lead_portal_leads a 
    left join lead_portal_leadsource b on a.lead_source_id = b.id
    left join (select lead_id, count(event_type) as attempted_count, max(created) as last_attempt_time 
    from lead_portal_leadevents where event_type in (5,7,8,9,10) group by 1) c on a.id = c.lead_id
    left join (select lead_id, count(event_type) as connected_count, max(created) as last_connected_time 
    from lead_portal_leadevents where event_type in (5,7,8) group by 1) d on a.id = d.lead_id
    left join (select lead_id, count(event_type) as interested_count, max(created) as last_interested_time 
    from lead_portal_leadevents where event_type in (5) group by 1) e on a.id = e.lead_id
    where a.lead_source_id in (4) and date(a.created) = '{dat1}' '''. format(dat1=date1)
leads = sql.read_sql(query,cnx_frodo)
leads


# In[55]:


query1 = '''
    select * from lead_portal_leads a
 left join (select lead_id, count(event_type) as attempted_count, max(created) as last_attempt_time
    from lead_portal_leadevents where event_type in (5,7,8,9,10) group by 1) c on a.id = c.lead_id
    left join (select lead_id, count(event_type) as connected_count, max(created) as last_connected_time
    from lead_portal_leadevents where event_type in (5,7,8) group by 1) d on a.id = d.lead_id
     left join (select lead_id, count(event_type) as interested_count, max(created) as last_interested_time
    from lead_portal_leadevents where event_type in (5) group by 1) e on a.id = e.lead_id
where lead_source_id=4 and date(created)>'2020-08-30'
order by date(created) desc'''
leads1 = sql.read_sql(query1,cnx_frodo)
leads1
 


# In[44]:


query2 = '''-- --yes_cache
    select  *
from jobs_knowlaritycalllog
where knowlarity_number like ('%9108516360%')
and created > '2020-08-30' '''
leads2 = sql.read_sql(query2,cnx_frodo)
leads2


# In[64]:


# date(a.created) = '{date1}' and .format(date1=date1)


# In[65]:


leads['referring_rider_id'] = pd.to_numeric(leads['referring_rider_id'])


# In[66]:


a = dt.datetime.now() - dt.timedelta(minutes=offset2)
a


# In[67]:


leads['modified'] = a
# leads['modified'] = leads['modified'].astype(datetime)
leads['modified'] = pd.to_datetime(leads['modified'],utc=True)
leads


# In[68]:


def priority(row):
    if (row['city_priority']==1 ):
        return 0
    elif (row['city_priority']==2 ):
        return 1
    elif (row['city_priority']==3 ):
        return 2
    elif (row['city_priority']==4 ):
        return 3
    elif (row['city_priority']==5 ):
        return 4
    elif (row['city_priority']==6 ):
        return 5
    else:
        return 6


# In[69]:


leads['overall_priority'] = leads.apply(priority,axis=1)


# In[70]:


leads


# In[71]:


leads['lead_datetime'] = leads['created']+dt.timedelta(minutes = offset4)


# In[72]:


leads['hour'] = leads['lead_datetime'].dt.hour
leads['date'] = leads['lead_datetime'].dt.day
leads.dtypes


# In[73]:


leads.rename(columns={'lead_id_x':'lead_id'}, inplace=True)


# In[74]:


leads


# In[75]:


def f(leads):
    a = leads['lead_id'].count()
#     b = leads.drop_duplicates('rider_id').rider_id.count()
#     c = leads.drop_duplicates('rider_id').mask_status.sum()
#     d = leads.drop_duplicates('rider_id').sanitizer_status.sum()
#     e = leads.drop_duplicates('rider_id').tshirt_status.sum()
#     f = leads.drop_duplicates('rider_id').Fully_groomed.sum()
    return pd.Series([a], index=['Total leads']) 

hourly_leads = (leads.groupby(['hour']).apply(f) )
hourly_leads


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[7]:


# a = '''select rider_id,vertical as referring_rider_vertical from partner_vertical'''

# vertical = sql.read_sql(a,cnx_swiggy)
# vertical


# In[8]:


# vertical = vertical.dropna()
# vertical


# In[9]:


# #vertical = vertical['referring_rider_id'].astype(int)
# # vertical = vertical['name'].astype('str')
# vertical['rider_id'] = pd.to_numeric(vertical['rider_id'])
# vertical


# In[10]:


# leads = leads.merge(vertical,how ='left', left_on=['referring_rider_id'], right_on=['rider_id'])
# leads


# In[11]:


query2 = '''select a.seller_city as city_id, b.name as city, 
sum(a.current_partner) as curr_count, sum(a.required_partner) req_count, 
sum(a.required_partner) - sum(a.current_partner) as gap from demand_supply_partnerrequirement a
left join RMS_city b on a.seller_city = b.id where a.status not in (2,4) and date(a.created) between '{dat5}' and '{date1}' group by 1,2'''.format(date1=date1,dat5=dat5)
demand = sql.read_sql(query2,cnx_rms)
demand


# In[14]:


# demand.dtypes


# In[15]:


# leads.dtypes


# In[16]:


demand['city_id'] = pd.to_numeric(demand['city_id'])


# In[17]:


leads['city_id'] = pd.to_numeric(leads['city_id'])


# In[18]:


leads = pd.merge(leads,demand,how='left', left_on='city_id', right_on='city_id')


# In[19]:


leads


# In[24]:


leads.columns


# In[20]:


# referring_rider_list = leads.loc[leads['referring_rider_id'].notnull()]
# referring_rider_list = tuple(list(pd.to_numeric(referring_rider_list['referring_rider_id'].unique())))
# print(len(referring_rider_list))


# In[20]:


# query3 = '''select referring_rider_id, id as lead_id, joined_rider_id, date(created) as lead_date, lead_status
# from lead_portal_leads 
# where referring_rider_id in {referring_rider_list} and date(created) 
# between '{dat_quality_check2}' 
# and '{dat_quality_check1}' '''.format(referring_rider_list=referring_rider_list, dat_quality_check1 = dat_quality_check1, dat_quality_check2 = dat_quality_check2)
# quality_leads = sql.read_sql(query3, cnx_frodo)
# print(len(quality_leads['lead_id']))


# In[21]:


# quality_check = quality_leads.loc[quality_leads['joined_rider_id'].notnull()]
# quality_check_list = tuple(list(pd.to_numeric(quality_check['joined_rider_id'].unique())))
# print(len(quality_check))


# In[ ]:


def priority(row):
    if (row['gap'] > 0):
        return 0
    elif (row['gap'] > 0):
        return 1
    elif (row['gap'] > 0 ):
        return 2
    elif (row['gap'] > 0 and row['quality_priority'] == 4):
        return 3
    else:
        return 4

